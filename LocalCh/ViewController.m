//
//  ViewController.m
//  LocalCh
//
//  Created by Gabriele Palma on 13/06/16.
//  Copyright © 2016 Gabriele Palma. All rights reserved.
//

#import "ViewController.h"
#import "DetailViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (instancetype)init {
    self = [super init];
    if (self) {
        self.currentLocation = [[CLLocation alloc] initWithLatitude:47.4 longitude:8.55];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"standardCell"];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShowNotification:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHideNotification:) name:UIKeyboardWillHideNotification object:nil];
}

// This never happens but I left it for completeness
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void) keyboardWillShowNotification:(NSNotification*) notification {
    CGRect frame  = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGRect keyboardFrame = [self.view convertRect:frame fromView:nil];
    self.bottomConstraint.constant = CGRectGetMaxY(self.view.bounds) - CGRectGetMinY(keyboardFrame);
    [self.view layoutIfNeeded];
}

// This never happens but I left it for completeness
- (void) keyboardWillHideNotification:(NSNotification*)notification {
    self.bottomConstraint.constant = 0;
    [self.view layoutIfNeeded];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.locator = [CLLocationManager new];
    [self.locator setDelegate:self];
    [self.locator setPausesLocationUpdatesAutomatically:NO];
    [self.locator setDesiredAccuracy:kCLLocationAccuracyKilometer];
    [self.locator requestWhenInUseAuthorization];
    [self.locator startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    [self.locator startUpdatingLocation];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// While most results are local, I've been unable to successfully limit the search exclusively to the specified region.
// The following threads on SO seems to highlight issues might due to the SDK itself.
// http://stackoverflow.com/questions/26884042/mklocalsearch-returning-results-outside-of-region
// http://stackoverflow.com/questions/13798804/use-mklocalsearch-to-search-for-locations-on-a-map
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    MKLocalSearchRequest* request = [[MKLocalSearchRequest alloc] init];
    request.naturalLanguageQuery = searchBar.text;
    request.region = MKCoordinateRegionMakeWithDistance(self.currentLocation.coordinate, 5000, 5000); // 5 km range from current location
    [self.previousRequest cancel];
    self.previousRequest = [[MKLocalSearch alloc] initWithRequest:request];
    __weak typeof(self) weakSelf = self;
    [self.previousRequest startWithCompletionHandler:^(MKLocalSearchResponse * _Nullable response, NSError * _Nullable error) {
        typeof(self) self = weakSelf;
        self.data = response;
        [self.tableView reloadData];
    }];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = [self.tableView dequeueReusableCellWithIdentifier:@"standardCell"];
    cell.textLabel.text = self.data.mapItems[indexPath.row].placemark.name;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.data.mapItems.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self performSegueWithIdentifier:@"detailSegue" sender:indexPath];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"detailSegue"]){
        DetailViewController* next = (DetailViewController*)segue.destinationViewController;
        NSIndexPath* ip = sender;
        next.sourceData = self.data.mapItems[ip.row];
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    CLLocation *currentLocation = locations.lastObject;
    NSTimeInterval howRecent = [currentLocation.timestamp timeIntervalSinceNow];
    if (fabs(howRecent) < 15.0) {
        [self.locator stopUpdatingLocation];
        self.currentLocation = currentLocation;
    }
}

@end
