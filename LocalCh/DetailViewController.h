//
//  DetailViewController.h
//  LocalCh
//
//  Created by Gabriele Palma on 13/06/16.
//  Copyright © 2016 Gabriele Palma. All rights reserved.
//

#import <UIKit/UIKit.h>
@import MapKit;

@interface DetailViewController : UIViewController

@property (nonatomic, readwrite, weak) IBOutlet UILabel* lblTitle;
@property (nonatomic, readwrite, weak) IBOutlet UILabel* lblPhone;
@property (nonatomic, readwrite, weak) IBOutlet UILabel* lblName;
@property (nonatomic, readwrite, weak) IBOutlet MKMapView* mapView;
@property (nonatomic, readwrite, strong) MKMapItem* sourceData;
@property (nonatomic, readwrite, strong) UIAlertController* sheetController;


@end
