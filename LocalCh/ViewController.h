//
//  ViewController.h
//  LocalCh
//
//  Created by Gabriele Palma on 13/06/16.
//  Copyright © 2016 Gabriele Palma. All rights reserved.
//

#import <UIKit/UIKit.h>
@import MapKit;
@import CoreLocation;

@interface ViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, CLLocationManagerDelegate>

@property (nonatomic, readwrite, strong) MKLocalSearch* previousRequest;
@property (nonatomic, readwrite, strong) CLLocationManager* locator;
@property (nonatomic, readwrite, weak) IBOutlet UITableView* tableView;
@property (nonatomic, readwrite, strong) MKLocalSearchResponse* data;
@property (nonatomic, readwrite, strong) CLLocation* currentLocation;
@property (nonatomic, readwrite, weak) IBOutlet NSLayoutConstraint* bottomConstraint;

@end

