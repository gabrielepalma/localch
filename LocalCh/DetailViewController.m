//
//  DetailViewController.m
//  LocalCh
//
//  Created by Gabriele Palma on 13/06/16.
//  Copyright © 2016 Gabriele Palma. All rights reserved.
//

#import "DetailViewController.h"

static NSString * const kGoogleMaps = @"comgooglemaps://";

@interface DetailViewController ()

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.lblName.text = self.sourceData.placemark.name;
    self.lblTitle.text = self.sourceData.placemark.title;
    self.lblPhone.text = self.sourceData.phoneNumber;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.mapView setRegion:MKCoordinateRegionMakeWithDistance(self.sourceData.placemark.coordinate, 5000, 5000)];
    MKPointAnnotation* annotation = [MKPointAnnotation new];
    [annotation setCoordinate:self.sourceData.placemark.coordinate];
    [self.mapView addAnnotation:annotation];
    [self.mapView selectAnnotation:annotation animated:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)mapPressed:(id)sender {
    self.sheetController = [UIAlertController alertControllerWithTitle:@"Open with:" message:@"Pick an app to open the map" preferredStyle:UIAlertControllerStyleActionSheet];
    __weak typeof(self) weakSelf = self;
    [self.sheetController addAction:[UIAlertAction actionWithTitle:@"Maps" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        typeof(self) self = weakSelf;
        [self.sourceData openInMapsWithLaunchOptions:nil];
    }]];
    if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:kGoogleMaps]]){
        [self.sheetController addAction:[UIAlertAction actionWithTitle:@"Google Maps" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            typeof(self) self = weakSelf;
            NSMutableString *url = [kGoogleMaps mutableCopy];
            [url appendString:[NSString stringWithFormat:@"?q=%f,%f&zoom=14", self.sourceData.placemark.coordinate.latitude, self.sourceData.placemark.coordinate.longitude]];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
        }]];
    }
    [self.sheetController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:self.sheetController animated:true completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
