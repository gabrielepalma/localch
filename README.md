LocalCh is a simple local search using MKLocalSearch SDK.

Changes since the on site interview (i) added geolocation, (ii) the tableview has been made aware of the keyboard, (iii) added point annotations (pins) on the detail map, (iv) added possibility to open the location on Maps or Google Maps by single tapping on the detail map, (v) minor adjustments to the detail panel